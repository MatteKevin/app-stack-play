
.PHONY: test apiDoc

test:
	vendor/phpunit/phpunit/phpunit test/app_stack_test.php

apiDoc:
	mkdir -p www/html/api
	phpDocumentor -d src -t www/html/api

publish:
	rm -fr /var/www/html/app-stack-play/*
	cp -pr www/html/* /var/www/html/app-stack-play/

<?php

require 'src/appStack.php';
use App\appStack;

class AppStackTest extends PHPUnit_Framework_TestCase
{
    public function testListTables()
    {
        $app = New AppStack;
        $result = $app->listTables();
        $this->assertEquals(2, count($result));
    }
}
